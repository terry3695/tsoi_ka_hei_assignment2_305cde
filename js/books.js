/*global angular  */

/* we 'inject' the ngRoute module into our app. This makes the routing functionality to be available to our app. */
var myApp = angular.module('myApp', ['ngRoute','ngCookies'])

/* the config function takes an array. */
myApp.config( ['$routeProvider', function($routeProvider) {
  $routeProvider
    .when('/search', {
		  templateUrl: 'templates/search.html',
      controller: 'searchController'
		})
    .when('/detail/:id', {
      templateUrl: 'templates/detail.html',
      controller: 'detailController'
    })
    .when('/register', {
		  templateUrl: 'templates/register.html',
      controller: 'registerController'
		})
    .when('/favourites', {
		  templateUrl: 'templates/favourites.html',
      controller: 'favouritesController'
		})
		.when('/login', {
		  templateUrl: 'templates/login.html',
      controller: 'loginController'
		})
		.otherwise({
		  redirectTo: 'search'
		})
	}])
	
myApp.factory('PageStorage',function(){
  var userDetail = {
    username: ""
  };
  return{
      setUsername : function(username)
        {
          userDetail.username = username;
        }
  };
});

myApp.controller('searchController', function($scope, $http, $cookies,$window) {

    var checking = $cookies.get('username') == undefined ? false : true;
    $scope.loginstatus = checking;
    
    console.log("logged in:" + $scope.loginstatus);
    
    if (checking == true) {
        $scope.userID = $cookies.get('username');
        console.log($cookies.get('username'));
    }else{
      $scope.Calllogin = true
    }

  
  $scope.message = 'This is the search screen'
  $scope.search = function($event) {
    console.log('search()')
    if ($event.which == 13) { // enter key presses
      var search = $scope.searchTerm
      console.log(search)
      var url = 'https://www.googleapis.com/books/v1/volumes?maxResults=20&fields=items(id,volumeInfo(title,authors,description,imageLinks(thumbnail)))&q='+search
      $http.get(url).success(function(response) {
        console.log(response)
        $scope.books = response.items
        //console.log($scope.books[0].volumeInfo.imageLinks.thumbnail)
        $scope.searchTerm = ''
      })
    }
  }
  
  $scope.logout = function() {
    alert("logout success")
     $cookies.remove('username');
     $scope.loginStatus = undefined
     window.location.reload()
   }
  
})

myApp.controller('registerController',function($scope, $http) {
  $scope.message = 'This is the user register page'
  $scope.register = function(){
    if($scope.username !== undefined && $scope.password !== undefined && $scope.email !== undefined){
      
      console.log("register")
        var data = {
          "username": $scope.username,
          "password": $scope.password,
          "email": $scope.email
        }
        
      console.log(data)
        var url = 'https://tsoi-ka-hei-assignment2-305cde-terry3695.c9users.io/createUser'
        
        $http.post(url,data).success(function(response) {
        console.log(response)
        alert(response)
        window.location.reload();
      })
      .error(function(response){
        alert(response)
      })
    }else{
    alert("please insert all data")
  }
      
  }
  
    
})

myApp.controller('loginController',function($scope, $http,$cookies) {
  

  
  $scope.login = function(){
    if($scope.username !== undefined && $scope.password !== undefined){
      
        var data = {
          "username": $scope.username,
          "password": $scope.password
        }
        
      console.log(data)
        var url = 'https://tsoi-ka-hei-assignment2-305cde-terry3695.c9users.io/login'
        
        $http.post(url,data).success(function(response) {
        console.log(response)
        alert(response)
        $cookies.put("username",data.username)
        console.log("cookies username is: " + $cookies.get('username'))
        window.location = "index.html";
      })
      .error(function(response){
        alert("please insert correct username and password")
      })
    }else{
    alert("please insert all data")
  }
      
  }
    
})


myApp.controller('detailController', function($scope, $routeParams ,$http,$cookies) {
  $scope.message = 'This is the detail screen'
  $scope.loginstatus = $cookies.get('username') == undefined ? false : true;
  $scope.id = $routeParams.id
  var id =  $scope.id;
      var url = 'https://www.googleapis.com/books/v1/volumes/'+id
      $http.get(url).success(function(response) {
        console.log(response)
        //console.log(response.volumeInfo.title)
        $scope.books = response

      })
  //console.log($scope.books)
  $scope.addToFavourites = function(id) {
    
        var data = {
          "username": $cookies.get('username'),
          "bookID": id,
          "title": $scope.books.volumeInfo.title
        }
      var url = 'https://tsoi-ka-hei-assignment2-305cde-terry3695.c9users.io/addFavourites'
        
        $http.post(url,data).success(function(response) {
        console.log(response);
        alert(response);

        window.location.reload();
        
      });
    
  }
  var array = [];
  var result = [];
  var url = 'https://tsoi-ka-hei-assignment2-305cde-terry3695.c9users.io/getReview'
      $http.get(url).success(function(response) {
        //var json = JSON.parse(response);
        console.log(response);
        array = response
        for(var i=0;i<array.length;i++){
          if(array[i].BookID == id){
            result.push(array[i]);
          }
          if(array[i].Username == $cookies.get('username')){
            $scope.userID = $cookies.get('username');
          }
        }
        //console.log(array.length);
        if(result.length == 0){
          console.log("no review");
          $scope.NoReviewValue = true
        }
        $scope.reviews = result;


      })
  
   $scope.deleteReview = function(reviewID) {
      var url = 'https://tsoi-ka-hei-assignment2-305cde-terry3695.c9users.io/removeReview/'+reviewID
      console.log(url);
       $http.delete(url).success(function(response) {
        alert(response)
        window.location.reload();
       });
   }
  
  $scope.createReview = function(){
    if($scope.review !== undefined){
      var data = {
          "username": $cookies.get('username'),
          "bookID": id,
          "review": $scope.review
          
        }
      var url = 'https://tsoi-ka-hei-assignment2-305cde-terry3695.c9users.io/createReview'
        
        $http.post(url,data).success(function(response) {
        console.log(response);
        alert(response);

        window.location.reload();
        
      });
    }
    else
    {
      alert("please insert review contents");
    }
  }
})
 
myApp.controller('favouritesController', function($scope,$http,$window,$cookies) {
  console.log('fav controller')
  $scope.message = 'This is the favourites screen'
  var checking = $cookies.get('username') == undefined ? false : true;
  if(checking == false){
    alert('Please login to the system');
    window.location = "index.html"
  }
  
  var array = [];
  var result = [];
  var url = 'https://tsoi-ka-hei-assignment2-305cde-terry3695.c9users.io/getFavourites'
      $http.get(url).success(function(response) {
        //var json = JSON.parse(response);
        console.log(response);
        array = response
        for(var i=0;i<array.length;i++){
          if(array[i].Username == $cookies.get('username')){
            result.push(array[i]);
          }
        }
        //console.log(array.length);
        if(result.length == 0){
          console.log("no favourites");
          $scope.NoFavourites = true
          
        }
        $scope.Favourites = result;
        

      })
  
  $scope.delete = function(FavouritesID) {
    var url = 'https://tsoi-ka-hei-assignment2-305cde-terry3695.c9users.io/removeFavourites/'+FavouritesID
      console.log(url);
       $http.delete(url).success(function(response) {
        alert(response)
        window.location.reload();
       });
  }
  
})
